package com.sxt.bus.service;

import java.util.Map;

import com.sxt.bus.vo.CheckVo;
import com.sxt.sys.utils.DataGridView;

public interface CheckService {
	/*
	 * 根据出租单号加载检查单的表单数据
	 */
	Map<String, Object> initCheckFormData(String rentid);
	/*
	 * 保存检查单数据
	 */
	void addCheck(CheckVo checkVo);
	/*
	 * 查询
	 */
	DataGridView queryAllCheck(CheckVo checkVo);
	/*
	 * 修改检查单
	 */
	void updateCheck(CheckVo checkVo);
}
