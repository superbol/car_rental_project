package com.sxt.bus.service;

import java.util.Map;

import com.sxt.bus.domain.Rent;
import com.sxt.bus.vo.RentVo;
import com.sxt.sys.utils.DataGridView;

/*
 * 汽车出租的服务接口
 */
public interface RentService {
	/*
	 * 保存出租单信息
	 */
	void addRent(RentVo rentVo);
	/*
	 * 查询
	 */
	DataGridView queryAllRent(RentVo rentVo);
	/*
	 * 删除
	 */
	void deleteRent(String rentid);
	/*
	 * 编辑
	 */
	void updateRent(RentVo rentVo);
	/*
	 * 根据出租单号查询出租单信息
	 */
	Rent queryRentByRentId(String rentId);
	
}
