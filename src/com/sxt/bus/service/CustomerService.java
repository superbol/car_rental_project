package com.sxt.bus.service;

import java.util.List;

import com.sxt.bus.domain.Customer;
import com.sxt.bus.vo.CustomerVo;
import com.sxt.sys.utils.DataGridView;


/*
 * 客户管理的服务接口
 */
public interface CustomerService {
	/*
	 * 查询所有客户
	 */
	public DataGridView queryAllCustomer(CustomerVo customerVo);
	
	/*
	 * 添加客户
	 */
	public void addCustomer(CustomerVo customerVo);
	/*
	 * 修改客户	 
	 */
	public void updateCustomer(CustomerVo customerVo);
	
	/*
	 * 删除客户
	 */
	public void deleteCustomer(String customerVoid);
	/*
	 * 批量删除客户
	 */
	public void deleteBatchCustomer(String[] ids);
	/*
	 * 根据身份证查询客户信息
	 */
	public Customer queryCustomerByIdentity(String identity);
	/*
	 * 查询客户数据返回集合
	 */
	public List<Customer> queryAllCustomerForList(CustomerVo customerVo);
}
