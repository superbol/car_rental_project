package com.sxt.bus.service;

import com.sxt.bus.domain.Car;
import com.sxt.bus.vo.CarVo;
import com.sxt.sys.utils.DataGridView;


/*
 * 车辆管理的服务接口
 */
public interface CarService {
	/*
	 * 查询所有车辆
	 */
	public DataGridView queryAllCar(CarVo carVo);
	
	/*
	 * 添加车辆
	 */
	public void addCar(CarVo carVo);
	/*
	 * 修改车辆	 
	 */
	public void updateCar(CarVo carVo);
	
	/*
	 * 删除车辆
	 */
	public void deleteCar(String carnumber);
	/*
	 * 批量删除车辆
	 */
	public void deleteBatchCar(String[] carnumbers);
	/*
	 * 根据车牌号查询
	 */
	public Car queryCarByCarNumber(String carnumber);
}
