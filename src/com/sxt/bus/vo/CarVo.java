package com.sxt.bus.vo;

import com.sxt.bus.domain.Car;
import com.sxt.bus.domain.Customer;
import com.sxt.sys.domain.User;
/*
 * 菜单增强类
 */
public class CarVo extends Car {
	/*
	 * 分页参数
	 */
	private Integer page;
	private Integer Limit;
	//接收多个角色id
	private String ids[];
	
	public String[] getIds() {
		return ids;
	}
	public void setIds(String[] ids) {
		this.ids = ids;
	}
	public Integer getPage() {
		return page;
	}
	public void setPage(Integer page) {
		this.page = page;
	}
	public Integer getLimit() {
		return Limit;
	}
	public void setLimit(Integer limit) {
		Limit = limit;
	}
	
	
}
