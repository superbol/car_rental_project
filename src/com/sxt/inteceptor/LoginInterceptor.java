package com.sxt.inteceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.sxt.sys.domain.User;

public class LoginInterceptor implements HandlerInterceptor {

	/*
	 * handler中处理请求的方法,完全执行完毕之后，执行该方法 对handler中的方法进行异常的统一处理 和 进行日志记录
	 */

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {

	}

	/*
	 * handler中处理请求的方法，返回 Modelandview对象之前执行对象Modelandview进行统一处理
	 */
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
	}

	/*
	 * handler中处理请求的方法之前执行
	 * 
	 * @return 控制者被拦截的handler是否进行放行
	 */

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		// 得到session
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		if (null != user) {
			// 登陆成功的用户，返回true之后，继续访问服务器发送的请求，即此时用户已经登录；
			return true;
		}
		// 否则重定向到登陆页面
		response.sendRedirect("../index.jsp");
		return false;

	}

}
