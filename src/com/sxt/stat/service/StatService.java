package com.sxt.stat.service;

import java.util.List;

import com.sxt.stat.domain.BaseEntity;

public interface StatService {
	/*
	 * 查询客户地区数据
	 */
	public List<BaseEntity> loadCustomerAreaStatList();
	/**
	 * 业务员年度统计数据
	 * @return
	 */
	public List<BaseEntity> loadOpernameYearGradeList(String year);
	/*
	 * 查询公司年度月份销售额
	 */
	public List<Double> loadCompanyYearGradeStatList(String year);
	
}
