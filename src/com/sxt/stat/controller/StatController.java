package com.sxt.stat.controller;

import static org.hamcrest.CoreMatchers.nullValue;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sxt.bus.domain.Customer;
import com.sxt.bus.domain.Rent;
import com.sxt.bus.service.CustomerService;
import com.sxt.bus.service.RentService;
import com.sxt.bus.vo.CustomerVo;
import com.sxt.stat.domain.BaseEntity;
import com.sxt.stat.service.StatService;
import com.sxt.stat.utils.ExportCustomerUtils;
import com.sxt.stat.utils.ExportRentUtils;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.LineCaptcha;

/*
 * 统计分析
 */
@RequestMapping("stat")
@Controller
public class StatController {
	
	@Autowired
	private StatService statService;
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private RentService rentService;
	
	/*
	 * 跳转到客户地区统计页面
	 */
	@RequestMapping("toCustomerAreaStat")
	public String toCustomerAreaStat() {
		return "stat/customerAreaStat";
	}
	/*
	 * 跳转到业务员年度销售额
	 */
	@RequestMapping("toOpernameYearGradeStat")
	public String toOpernameYearGradeStat() {
		return "stat/opernameYearGradeStat";
	}
	/*
	 * 跳转到公司年度统计
	 */
	@RequestMapping("toCompanyYearGradeStat")
	public String toLadcompanyYearGradeStat() {
		return "stat/componyYearGradeStat";
	}
	
	/*
	 * 加载客户地区统计
	 */
	@RequestMapping("loadCustomerAreaStatJson")
	@ResponseBody
	public List<BaseEntity> loadCustomerAreaStatJson(){
		return this.statService.loadCustomerAreaStatList();
	}
	/*
	 * 加载客户地区统计
	 */
	@RequestMapping("loadOpernameYearGradeStat")
	@ResponseBody
	public Map<String, Object> loadOpernameYearGradeStat(String year){
		List<BaseEntity> entities = this.statService.loadOpernameYearGradeList(year);
		Map<String, Object> map = new HashMap<>();
		List<String> names = new ArrayList<>();
		List<Double> values = new ArrayList<>();
		for (BaseEntity baseEntity : entities) {
			names.add(baseEntity.getName());
			values.add(baseEntity.getValue());
		}
		map.put("name", names);
		map.put("value", values);
		return map;
	}
	/*
	 * 加载公司年度月份销售额
	 */
	@RequestMapping("loadCompanyYearGradeStat")
	@ResponseBody
	public List<Double> toLoadCompanyYearGradeStat(String year){
		List<Double> entities = this.statService.loadCompanyYearGradeStatList(year);
		for (int i = 0; i < entities.size(); i++) {
			if (null==entities.get(i)) {
				entities.set(i, 0.0);
			}
		}
		return entities;
	}
	
	/*
	 * 导出客户数据
	 */
	@RequestMapping("exportCustomer")
	public ResponseEntity<Object> exportCustomer(CustomerVo customerVo,HttpServletResponse response) {
		List<Customer> customers =  customerService.queryAllCustomerForList(customerVo);
		String filename  = "客户数据.xls";
		String sheetname  = "客户数据";
		ByteArrayOutputStream bos = ExportCustomerUtils.exportCustomer(customers,sheetname);
		
		try {
			filename=URLEncoder.encode(filename,"UTF-8");
			//创建封装响应头信息的对象
			HttpHeaders header = new HttpHeaders();
			//封装响应内容类型(APPLICATION_OCTET_STREAM 响应的内容不限定)
			header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			//设置下载的文件名称
			header.setContentDispositionFormData("attachment", filename);
			return new ResponseEntity<Object>(bos.toByteArray(),header,HttpStatus.CREATED);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	/*
	 * 导出出租单
	 */
	@RequestMapping("exportRent")
	public ResponseEntity<Object> exportRent(String rentid) {
		//根据出租单号查询出租单信息
		Rent rent = rentService.queryRentByRentId(rentid);
		//根据身份证号查询客户信息
		Customer customer = customerService.queryCustomerByIdentity(rent.getIdentity());
		
		String filename  =customer.getCustname() +"的出租单.xls";
		String sheetname  = customer.getCustname()+"的出租单";
		ByteArrayOutputStream bos = ExportRentUtils.exportCustomer(rent,customer, sheetname);
		
		try {
			filename=URLEncoder.encode(filename,"UTF-8");
			//创建封装响应头信息的对象
			HttpHeaders header = new HttpHeaders();
			//封装响应内容类型(APPLICATION_OCTET_STREAM 响应的内容不限定)
			header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			//设置下载的文件名称
			header.setContentDispositionFormData("attachment", filename);
			return new ResponseEntity<Object>(bos.toByteArray(),header,HttpStatus.CREATED);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	
}
