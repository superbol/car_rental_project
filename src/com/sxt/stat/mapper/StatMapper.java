package com.sxt.stat.mapper;

import java.util.List;

import com.sxt.stat.domain.BaseEntity;

public interface StatMapper {
	/*
	 * 查询客户地区数据
	 */
	List<BaseEntity> queryCustomerAreaStat();
	/*
	 * 业务员年度统计数据
	 */
	List<BaseEntity> queryLoadOpernameYearGradeList(String year);
	/*
	 * 查询公司年度月份销售额
	 */
	List<Double> queryCompanyYearGradeStat(String year);
}
