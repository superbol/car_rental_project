package com.sxt.sys.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.sxt.sys.domain.Role;

public interface RoleMapper {
    int deleteByPrimaryKey(Integer roleid);

    int insert(Role record);

    int insertSelective(Role record);

    Role selectByPrimaryKey(Integer roleid);

    int updateByPrimaryKeySelective(Role record);

    int updateByPrimaryKey(Role record);
    
    /*
     * 查询所有角色
     */
    List<Role> queryAllRole(Role role);
    /*
     * 根据角色ID删除sys_role_role的数据
     */
	void deleteRoleMenuByRid(Integer roleId);
	/*
	 * 根据角色ID删除sys_role_user里面的数据
	 */
	void deleteRoleUserByRid(Integer roleId);
	/*
	 * 根据用户ID删除sys_role_user里面的数据
	 */
	void deleteRoleUserByUid(Integer userid);
	/*
	 * 保存角色与菜单的关系 sys_role_menu
	 */
	void insertRoleMenu(@Param("rid")Integer rid,@Param("mid")Integer mid);
	/*
	 * 根据用户id查询已拥有的角色
	 */
	List<Role> queryRoleByUid(@Param("available")Integer available,@Param("uid")Integer userid);
}