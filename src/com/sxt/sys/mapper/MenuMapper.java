package com.sxt.sys.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.sxt.sys.domain.Menu;

public interface MenuMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Menu record);

    int insertSelective(Menu record);

    Menu selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Menu record);

    int updateByPrimaryKey(Menu record);
    
    /*
     * 查询所有菜单
     */
    List<Menu> queryAllMenu(Menu menu);
    /*
     * 根据Pid查询菜单
     */
	Integer queryMenuByPid(@Param("pid")Integer pid);
	/*
	 * 根据菜单ID删除sys_role_menu的数据
	 */
	void deleteRoleMenuByMid(@Param("mid")Integer mid);
	/*
	 * 根据角色id 查询菜单
	 */
	List<Menu> queryMenuByRoleId(@Param("available")Integer available,@Param("rid")Integer roleid);
	/*
	 * 根据用户ID查询菜单
	 */
	List<Menu> queryMenuByUid(@Param("available")Integer available,@Param("uid") Integer userId);
    
}