package com.sxt.sys.vo;

import com.sxt.sys.domain.User;

/*
 * 菜单增强类
 */
public class UserVo extends User {
	/*
	 * 分页参数
	 */
	private Integer page;
	private Integer Limit;
	// 接收多个角色id
	private Integer ids[];

	private String code;

//	/*
//	 * 时间
//	 */
//	@DateTimeFormat(pattern = "yyyy-MM-dd")
//	private Date brithtime;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer[] getIds() {
		return ids;
	}

	public void setIds(Integer[] ids) {
		this.ids = ids;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getLimit() {
		return Limit;
	}

	public void setLimit(Integer limit) {
		Limit = limit;
	}

}
