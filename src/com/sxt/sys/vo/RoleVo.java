package com.sxt.sys.vo;

import com.sxt.sys.domain.Role;

public class RoleVo extends Role {
	/*
	 * 分页参数
	 */
	private Integer page;
	private Integer Limit;
	//接收多个角色id
	private Integer ids[];
	
	public Integer[] getIds() {
		return ids;
	}
	public void setIds(Integer[] ids) {
		this.ids = ids;
	}
	public Integer getPage() {
		return page;
	}
	public void setPage(Integer page) {
		this.page = page;
	}
	public Integer getLimit() {
		return Limit;
	}
	public void setLimit(Integer limit) {
		Limit = limit;
	}
	
	
}
