package com.sxt.sys.vo;

import com.sxt.sys.domain.Menu;

public class MenuVo extends Menu {
	/*
	 * 分页参数
	 */
	private Integer page;
	private Integer Limit;
	public Integer getPage() {
		return page;
	}
	public void setPage(Integer page) {
		this.page = page;
	}
	public Integer getLimit() {
		return Limit;
	}
	public void setLimit(Integer limit) {
		Limit = limit;
	}
	
	
}
