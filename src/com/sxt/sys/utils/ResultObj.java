package com.sxt.sys.utils;

import com.sxt.sys.constast.SysConstast;

public class ResultObj {
	private Integer code = 0;
	private String msg;
	// 注册失败与成功
	public static final ResultObj SIGNUP_SUCCESS = new ResultObj(SysConstast.CODE_SUCESSS, SysConstast.SIGNUP_SUCCESS);
	public static final ResultObj SIGNUP_ERROR = new ResultObj(SysConstast.CODE_SUCESSS, SysConstast.SIGNUP_ERROR);
	// 添加失败与成功
	public static final ResultObj ADD_SUCCESS = new ResultObj(SysConstast.CODE_SUCESSS, SysConstast.ADD_SUCCESS);
	public static final ResultObj ADD_ERROR = new ResultObj(SysConstast.CODE_SUCESSS, SysConstast.ADD_ERROR);
	// 更新添加失败与成功
	public static final ResultObj UPDATE_SUCCESS = new ResultObj(SysConstast.CODE_SUCESSS, SysConstast.UPDATE_SUCCESS);
	public static final ResultObj UPDATE_ERROR = new ResultObj(SysConstast.CODE_SUCESSS, SysConstast.UPDATE_ERROR);
	// 删除失败与成功
	public static final ResultObj DELETE_SUCCESS = new ResultObj(SysConstast.CODE_SUCESSS, SysConstast.DELETE_SUCCESS);
	public static final ResultObj DELETE_ERROR = new ResultObj(SysConstast.CODE_SUCESSS, SysConstast.DELETEE_ERROR);
	// 分配失败与成功
	public static final ResultObj DISPATCH_SUCCESS = new ResultObj(SysConstast.CODE_SUCESSS,
			SysConstast.DISPATCH_SUCCESS);
	public static final ResultObj DISPATCH_ERROR = new ResultObj(SysConstast.CODE_SUCESSS, SysConstast.DISPATCH_ERROR);
	// 重置失败与成功
	public static final ResultObj RESET_SUCCESS = new ResultObj(SysConstast.CODE_SUCESSS, SysConstast.RESET_SUCCESS);
	public static final ResultObj RESET_ERROR = new ResultObj(SysConstast.CODE_SUCESSS, SysConstast.RESET_ERROR);
	// 状态码0与-1
	public static final ResultObj STATUS_TRUE = new ResultObj(SysConstast.CODE_SUCESSS);
	public static final ResultObj STATUS_FALSE = new ResultObj(SysConstast.CODE_ERROR);
	// 密码正确和错误
	public static final ResultObj PWD_SUCCESS = new ResultObj(SysConstast.CODE_SUCESSS, SysConstast.PWD_SUCCESS);
	public static final ResultObj PWD_ERROR = new ResultObj(SysConstast.CODE_SUCESSS, SysConstast.PWD_ERROR);
	// 验证码不正确
	public static final ResultObj USER_SIGNUP_CODE_ERROR_MSG = new ResultObj(SysConstast.CODE_SUCESSS,
			SysConstast.USER_SIGNUP_CODE_ERROR_MSG);
	// 用户已存在
	public static final ResultObj USER_EXIST = new ResultObj(SysConstast.CODE_SUCESSS, SysConstast.USER_EXIST);
//	public static final ResultObj USER_NOT_EXIST = new ResultObj(SysConstast.CODE_SUCESSS, SysConstast.USER_NOT_EXIST);

	private ResultObj(Integer code, String msg) {
		super();
		this.code = code;
		this.msg = msg;
	}

	private ResultObj(Integer code) {
		this.code = code;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
