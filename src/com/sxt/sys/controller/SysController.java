package com.sxt.sys.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("sys")
public class SysController {

	/*
	 * 跳转到用户注册
	 */
	@RequestMapping("toSignup")
	public String toSignup() {
		return "system/main/signup";
	}

	/*
	 * 跳转到用户管理
	 */
	@RequestMapping("toMenuManager")
	public String toMenuManager() {
		return "system/menu/menuManager";
	}

	/*
	 * 跳转菜单管理左边的菜单树
	 */
	@RequestMapping("toMenuLeft")
	public String toMenuLeft() {
		return "system/menu/menuLeft";
	}

	/*
	 * 跳转菜单管理右边的菜单
	 */
	@RequestMapping("toMenuRight")
	public String toMenuRight() {
		return "system/menu/menuRight";
	}

	/*
	 * 跳转菜单管理右边的菜单
	 */
	@RequestMapping("toRoleManager")
	public String toRoleManager() {
		return "system/role/roleManager";
	}

	/*
	 * 跳转到角色管理
	 */
	@RequestMapping("toUserManager")
	public String toUserManager() {
		return "system/user/userManager";
	}

	/*
	 * 跳转到日志管理
	 */
	@RequestMapping("toLogInfoManager")
	public String toLogInfoManager() {
		return "system/logInfo/logInfoManager";
	}

	/*
	 * 跳转到公告管理页面
	 */
	@RequestMapping("toNewsManager")
	public String toNewsManager() {
		return "system/news/newsManager";
	}

	/*
	 * 跳转到个人资料
	 */
	@RequestMapping("toUserInfo")
	public String toUserInfo() {
		return "system/user/userInfo";
	}

	/*
	 * 跳转到修改密码
	 */
	@RequestMapping("toChangePwd")
	public String toChangePwd() {
		return "system/user/changePwd";
	}

	/*
	 * 跳转到翻译接口测试
	 */
	@RequestMapping("toBaidu")
	public String toBaidu() {
		return "demo/baidu";
	}

	/*
	 * 跳转到文件上传
	 */
	@RequestMapping("toFilesupLoad")
	public String toFilesupLoad() {
		return "demo/filesUpLoad";
	}
}
