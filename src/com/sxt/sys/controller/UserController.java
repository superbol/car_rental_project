package com.sxt.sys.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sxt.sys.constast.SysConstast;
import com.sxt.sys.domain.User;
import com.sxt.sys.service.UserService;
import com.sxt.sys.utils.AppFileUtils;
import com.sxt.sys.utils.DataGridView;
import com.sxt.sys.utils.ResultObj;
import com.sxt.sys.utils.WebUtils;
import com.sxt.sys.vo.UserVo;

/**
 * 用户管理控制器
 * 
 * @author LJH
 *
 */
@RestController
@RequestMapping("user")
public class UserController {

	@Autowired
	private UserService userService;

	/*
	 * 加载用户列表返回DataGridView
	 */
	@RequestMapping("loadAllUser")
	public DataGridView loadAllmeenu(UserVo userVo) {
		return this.userService.queryAllUser(userVo);
	}

	/*
	 * 注册用户--查询用户名是否存在
	 */
	@RequestMapping("queryLoginName")
	public ResultObj queryLoginName(UserVo userVo) {
		Integer count = this.userService.queryLoginName(userVo.getLoginname());
		if (count != 0) {
			return ResultObj.USER_EXIST;
		}
		return null;
	}

	/*
	 * 注册用户
	 */
	@RequestMapping("signup")
	public ResultObj signup(UserVo userVo) {
		try {
			String code = WebUtils.getHttpSession().getAttribute("code").toString();
			if (userVo.getCode().toLowerCase().equals(code)) {
				this.userService.signUpUser(userVo);
				return ResultObj.SIGNUP_SUCCESS;
			} else {
				return ResultObj.USER_SIGNUP_CODE_ERROR_MSG;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ResultObj.SIGNUP_ERROR;
		}
	}

	/*
	 * 添加用户
	 */
	@RequestMapping("addUser")
	public ResultObj addUser(UserVo userVo) {
		try {
			this.userService.addUser(userVo);
			return ResultObj.ADD_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ResultObj.ADD_ERROR;
		}
	}

	/*
	 * 修改用户
	 */
	@RequestMapping("updateUser")
	public ResultObj updateUser(UserVo userVo) {
		try {
			this.userService.updateUser(userVo);
			return ResultObj.UPDATE_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ResultObj.UPDATE_ERROR;
		}
	}

	/*
	 * 删除用户
	 */
	@RequestMapping("deleteUser")
	public ResultObj deleteUser(UserVo userVo) {
		System.out.println(userVo);
		System.out.println(userVo.getUserid());
		try {
			this.userService.deleteUser(userVo.getUserid());
			return ResultObj.DELETE_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ResultObj.DELETE_ERROR;
		}
	}

	/*
	 * 批量删除用户
	 */
	@RequestMapping("deleteBatchUser")
	public ResultObj deleteBatchUser(UserVo userVo) {
		try {
			this.userService.deleteBatchUser(userVo.getIds());
			return ResultObj.DELETE_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ResultObj.DELETE_ERROR;
		}
	}

	/*
	 * 重置用户密码
	 */
	@RequestMapping("resetUserPwd")
	public ResultObj resetUserPwd(UserVo userVo) {
		try {
			this.userService.resetUserPwd(userVo.getUserid());
			return ResultObj.RESET_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ResultObj.RESET_ERROR;
		}
	}

	/*
	 * 加载用户管理分配的分配角色的数据
	 */
	@RequestMapping("initUserRole")
	public DataGridView initUserRole(UserVo userVo) {
		return this.userService.queryUserRole(userVo.getUserid());
	}

	/*
	 * 保存用户和角色的关系
	 */
	@RequestMapping("saveUserRole")
	public ResultObj saveUserRole(UserVo userVo) {
		try {
			this.userService.saveUserRole(userVo);
			return ResultObj.DISPATCH_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ResultObj.DISPATCH_ERROR;
		}
	}

	/*
	 * 修改个人信息
	 */
	@RequestMapping("updateUserInfo")
	public ResultObj updateUserInfo(UserVo userVo) {
		try {
			String headimg = userVo.getHeadimg();
			if (headimg.endsWith(SysConstast.FILE_UPLOAD_TEMP)) {
				String filePath = AppFileUtils.updateFileName(headimg, SysConstast.FILE_UPLOAD_TEMP);
				userVo.setHeadimg(filePath);
				// 把原来的删除
				User user = this.userService.queryUserInfo(userVo.getUserid());
				AppFileUtils.removeFileByPath(user.getHeadimg());
			}
			this.userService.updateUserInfo(userVo);
			return ResultObj.UPDATE_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ResultObj.UPDATE_ERROR;
		}
	}

	/*
	 * 查询个人信息
	 */
	@RequestMapping("queryUserInfo")
	public User queryUserInfo() {
		User user = (User) WebUtils.getHttpSession().getAttribute("user");
		user = this.userService.queryUserInfo(user.getUserid());
		return user;
	}

	/*
	 * 查询密码
	 */
	@RequestMapping("queryPwd")
	public ResultObj queryPwd(UserVo userVo) {
		User user = (User) WebUtils.getHttpSession().getAttribute("user");
		user = this.userService.queryUserInfo(user.getUserid());
		// 生成密文
		String pwd = DigestUtils.md5DigestAsHex(userVo.getPwd().getBytes());
		userVo.setPwd(pwd);
		if (userVo.getPwd().equals(user.getPwd())) {
			return ResultObj.PWD_SUCCESS;
		} else {
			return ResultObj.PWD_ERROR;
		}
	}

	/*
	 * 更改密码之查询密码
	 */
	@RequestMapping("updatePwd")
	public ResultObj updatePwd(UserVo userVo) {
		try {
			User user = (User) WebUtils.getHttpSession().getAttribute("user");
			// 生成密文
			String pwd = DigestUtils.md5DigestAsHex(userVo.getPwd().getBytes());
			userVo.setUserid(user.getUserid());
			userVo.setPwd(pwd);
			// 修改密码
			this.userService.updateUser(userVo);
			return ResultObj.UPDATE_SUCCESS;
		} catch (Exception e) {
			return ResultObj.UPDATE_ERROR;
		}
	}
}
