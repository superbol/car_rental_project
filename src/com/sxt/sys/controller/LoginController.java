package com.sxt.sys.controller;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sxt.sys.constast.SysConstast;
import com.sxt.sys.domain.User;
import com.sxt.sys.service.LogInfoService;
import com.sxt.sys.service.UserService;
import com.sxt.sys.utils.WebUtils;
import com.sxt.sys.vo.LogInfoVo;
import com.sxt.sys.vo.UserVo;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.LineCaptcha;
import cn.hutool.core.lang.Console;

/*
 * 用户登录控制器
 */
@Controller
@RequestMapping("login")
public class LoginController {
	@Autowired
	private UserService userService;

	@Autowired
	private LogInfoService logInfoService;

	/*
	 * 跳转到登录页面
	 */
	@RequestMapping("toLogin")
	public String tologin() {
		return "system/main/login";
	}

	/*
	 * 登录方法
	 */
	@RequestMapping("login")
	public String login(UserVo userVo, Model model) {
		// 最外层的if是判断session是否为空 为空就跳转到登录界面
		// uservo不可能为空的，只是里面的属性可能为空 所以要判断session是否为空 就要判断里面的属性 而不是判断userVo本身 (坑!)
		if (null != userVo.getLoginname()) {
//			String code = WebUtils.getHttpSession().getAttribute("code").toString();
//			if (userVo.getCode().toLowerCase().equals(code)) {
			User user = this.userService.login(userVo);
			if (null != user) {
				// 放入session
				WebUtils.getHttpSession().setAttribute("user", user);
				WebUtils.getHttpSession().setMaxInactiveInterval(240 * 60);
				// 记录登录日志 向sys_login_log插入数据

				LogInfoVo logInfoVo = new LogInfoVo();
				logInfoVo.setLoginname(user.getRealname() + "-" + user.getLoginname());
				logInfoVo.setLogintime(new Date());
				// 外网Ip:WebUtils.getHttpServletRequest().getRemoteAddr()
				// 内网ip:
				InetAddress addr;
				try {
					addr = (InetAddress) InetAddress.getLocalHost();
					logInfoVo.setLoginip(addr.getHostAddress().toString());// 获取Id地址
				} catch (UnknownHostException e) {
					e.printStackTrace();
				}
				// 添加
				logInfoService.addLogInfo(logInfoVo);
				return "system/main/index";

			} else {
				model.addAttribute("error", SysConstast.USER_LOGIN_ERROR_MSG);
				return "system/main/login";
			}

//			} else {
//				model.addAttribute("error", SysConstast.USER_LOGIN_CODE_ERROR_MSG);
//				return "system/main/login";
//			}

		} else {
			return "system/main/login";
		}

	};

	/**
	 * 得到登录验证码
	 * @throws IOException 
	 */
	@RequestMapping("getCode")
	public void getCode(HttpServletResponse response, HttpSession session) throws IOException {
		// 定义图形验证码的长和宽
		LineCaptcha lineCaptcha = CaptchaUtil.createLineCaptcha(116, 36, 4, 50);
		Console.log(lineCaptcha.getCode());
		session.setAttribute("code", lineCaptcha.getCode());
		ServletOutputStream outputStream = response.getOutputStream();
		ImageIO.write(lineCaptcha.getImage(), "JPEG", outputStream);
	}
}
