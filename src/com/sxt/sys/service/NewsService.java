package com.sxt.sys.service;

import java.util.List;

import com.sxt.sys.domain.News;
import com.sxt.sys.utils.DataGridView;
import com.sxt.sys.vo.NewsVo;

/*
 * 公告管理的服务接口
 */
public interface NewsService {
	/*
	 * 查询所有公告
	 */
	public DataGridView queryAllNews(NewsVo newsVo);
	
	/*
	 * 添加公告
	 */
	public void addNews(NewsVo newsVo);
	/*
	 * 修改公告	 
	 */
	public void updateNews(NewsVo newsVo);
	
	/*
	 * 删除公告
	 */
	public void deleteNews(Integer newsVoid);
	/*
	 * 批量删除公告
	 */
	public void deleteBatchNews(Integer[] ids);
	/*
	 * 根据id查询一个公告
	 */
	public News queryNewsById(Integer id);
	
}
