package com.sxt.sys.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sxt.sys.domain.Menu;
import com.sxt.sys.mapper.MenuMapper;
import com.sxt.sys.service.MenuService;
import com.sxt.sys.utils.DataGridView;
import com.sxt.sys.vo.MenuVo;

@Service
public class MenuServiceImpl implements MenuService {
	
	@Autowired
	private MenuMapper menuMapper;
	
	@Override
	public List<Menu> queryAllMenuForList(MenuVo menuVo) {
		return menuMapper.queryAllMenu(menuVo);
	}
	
	/*
	 * 后续得改 此方法是查询用户id 来决定开放什么功能菜单
	 */
	@Override
	public List<Menu> queryMenuByUserIdForList(MenuVo menuVo,Integer userId) {
		return menuMapper.queryMenuByUid(menuVo.getAvailable(),userId);
	}
	
	@Override
	public DataGridView queryAllMenu(MenuVo menuVo) {
		Page<Object> page = PageHelper.startPage(menuVo.getPage(), menuVo.getLimit());
		List<Menu> data = this.menuMapper.queryAllMenu(menuVo);
		System.out.println(data);
		System.out.println(page.getTotal());
		return new DataGridView(page.getTotal(),data);
	}

	@Override
	public void addMenu(MenuVo menuVo) {
		this.menuMapper.insertSelective(menuVo);
	}

	public void updateMenu(MenuVo menuVo) {
		this.menuMapper.updateByPrimaryKeySelective(menuVo);
		
	}

	@Override
	public Integer queryMenuByPid(Integer pid) {
		return this.menuMapper.queryMenuByPid(pid);
	}

	@Override
	public void deleteMenu(MenuVo menuVo) {
		//删除菜单表的数据
		this.menuMapper.deleteByPrimaryKey(menuVo.getId());
		//根据菜单ID删除sys_role_menu的数据
		this.menuMapper.deleteRoleMenuByMid(menuVo.getId());
		
	}

}
