package com.sxt.sys.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sxt.sys.domain.News;
import com.sxt.sys.mapper.NewsMapper;
import com.sxt.sys.service.NewsService;
import com.sxt.sys.utils.DataGridView;
import com.sxt.sys.vo.NewsVo;
@Service
public class NewsServicImpl implements NewsService {
	@Autowired
	private NewsMapper newsMapper;
	
	@Override
	public DataGridView queryAllNews(NewsVo news) {
		Page<Object> page = PageHelper.startPage(news.getPage(), news.getLimit());
		List<News> data = this.newsMapper.queryAllNews(news);
		return new DataGridView(page.getTotal(), data);
	}

	@Override
	public void addNews(NewsVo news) {
		this.newsMapper.insertSelective(news);
	}

	@Override
	public void deleteNews(Integer newsid) {
		this.newsMapper.deleteByPrimaryKey(newsid);
	}

	@Override
	public void deleteBatchNews(Integer[] ids) {
		for (Integer integer : ids) {
			deleteNews(integer);
		}
	}

	@Override
	public void updateNews(NewsVo newsVo) {
		this.newsMapper.updateByPrimaryKeySelective(newsVo);
	}

	@Override
	public News queryNewsById(Integer id) {
		return this.newsMapper.selectByPrimaryKey(id);
	}

}
