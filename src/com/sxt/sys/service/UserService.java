package com.sxt.sys.service;
/*
 * 用户服务接口
 */

import com.sxt.sys.domain.User;
import com.sxt.sys.utils.DataGridView;
import com.sxt.sys.vo.UserVo;

public interface UserService {
	/*
	 * 登录
	 */
	User login(UserVo userVo);

	/*
	 * 查询所有用户
	 */
	public DataGridView queryAllUser(UserVo userVo);

	/*
	 * 添加用户
	 */
	public void addUser(UserVo userVo);

	/*
	 * 添加用户
	 */
	public void signUpUser(UserVo userVo);

	/*
	 * 更新用户
	 */
	public void updateUser(UserVo userVo);

	/*
	 * 删除用户
	 */
	public void deleteUser(Integer userId);

	/*
	 * 批量删除用户
	 */
	public void deleteBatchUser(Integer[] ids);

	/*
	 * 重置密码
	 */
	public void resetUserPwd(Integer userId);

	/*
	 * 加载用户管理分配的分配角色的数据
	 */
	DataGridView queryUserRole(Integer userid);

	/*
	 * 保存用户和角色的关系
	 */
	void saveUserRole(UserVo userVo);

	/*
	 * 修改个人信息
	 */
	void updateUserInfo(UserVo userVo);

	/*
	 * 查询个人信息
	 */
	User queryUserInfo(Integer userid);

	/*
	 * 查询用户名是否存在
	 */
	Integer queryLoginName(String loginname);
}
