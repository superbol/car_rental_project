package com.sxt.sys.service;

import java.util.List;

import com.sxt.sys.domain.Role;
import com.sxt.sys.utils.DataGridView;
import com.sxt.sys.vo.RoleVo;

/*
 * 角色管理的服务接口
 */
public interface RoleService {
	/*
	 * 查询所有角色返回list<role>
	 */
	public List<Role> queryAllRoleForList(RoleVo rolevo);
	
	/*
	 * 根据用户id查询用户的可用角色
	 */
	public List<Role> queryRoleByUserIdForList(RoleVo roleVo,Integer userId);
	
	/*
	 * 查询所有角色
	 */
	public DataGridView queryAllRole(RoleVo roleVo);
	
	/*
	 * 添加角色
	 */
	public void addRole(RoleVo roleVo);
	/*
	 * 更新角色
	 */
	public void updateRole(RoleVo roleVo);
	
	/*
	 * 删除角色
	 */
	public void deleteRole(Integer roleId);
	/*
	 * 批量删除角色
	 */
	public void deleteBatchRole(Integer[] ids);
	/*
	 * 加载角色管理分配菜单的JSON
	 */
	public DataGridView initRoleMenuTreeJson(Integer roleid);
	/*
	 * 保存角色与菜单的关系
	 */
	public void saveRoleMenu(RoleVo roleVo);
	
}
