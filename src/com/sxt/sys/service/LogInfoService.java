package com.sxt.sys.service;

import java.util.List;

import com.sxt.sys.domain.LogInfo;
import com.sxt.sys.utils.DataGridView;
import com.sxt.sys.vo.LogInfoVo;

/*
 * 日志管理的服务接口
 */
public interface LogInfoService {
	/*
	 * 查询所有日志
	 */
	public DataGridView queryAllLogInfo(LogInfoVo logInfoVo);
	
	/*
	 * 添加日志
	 */
	public void addLogInfo(LogInfoVo logInfoVo);
	
	/*
	 * 删除日志
	 */
	public void deleteLogInfo(Integer logInfoId);
	/*
	 * 批量删除日志
	 */
	public void deleteBatchLogInfo(Integer[] ids);
	
}
