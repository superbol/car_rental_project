<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="loginHtml">
<head>
	<meta charset="utf-8">
	<title>登录-汽车出租系统</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="icon" href="${ctx}/resources/favicon.ico">
	<link rel="stylesheet" href="${ctx}/resources/layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="${ctx}/resources/css/public.css" media="all" />
</head>
<body class="loginBody">
	<!-- 登录 -->
	<form class="layui-form" id="loginFrm" method="post" action="${ctx }/login/login.action">
		<div class="login_face"><img src="${ctx}/resources/images/face.jpg" class="userAvatar"></div>
		<div class="layui-form-item input-item">
			<label for="userName">用户名</label>
			<input type="text" placeholder="请输入用户名" autocomplete="off" name="loginname" class="layui-input" lay-verify="required">
		</div>
		<div class="layui-form-item input-item">
			<label for="password">密码</label>
			<input type="password" placeholder="请输入密码" autocomplete="off" name="pwd" class="layui-input" lay-verify="required">
		</div>
<!-- 		<div class="layui-form-item input-item" id="imgCode"> -->
<!-- 			<label for="code">验证码</label> -->
<!-- 			<input type="text" placeholder="请输入验证码"  autocomplete="off" name="code" id="code" class="layui-input" > -->
<%-- 			<img src="${ctx}/login/getCode.action" onclick="this.src=this.src+'?'"> --%>
<!-- 		</div> -->
		<div class="layui-form-item">
			<button class="layui-btn layui-block" lay-filter="login" lay-submit>登录</button>
		</div>
		<div class="layui-form-item">
			<button type="button" class="layui-btn layui-block layui-btn-warm" id="signup" >注册</button>
		</div>
		<div class="layui-form-item " style="text-align: center;color: red;">
			${error }
		</div>
	</form>
	
	
	</div>
	
	<script type="text/javascript" src="${ctx}/resources/layui/layui.js"></script>
	<script type="text/javascript" src="${ctx}/resources/js/cache.js"></script>
	<script type="text/javascript">
	layui.use(['jquery', 'layer', 'form', 'table'],function(){
	    var form = layui.form,
	        layer = parent.layer === undefined ? layui.layer : top.layer
// 		 layer = layui.layer;
	      var  $ = layui.jquery;
	    	var table = layui.table;
	    //登录按钮
	    form.on("submit(login)",function(data){
	        $(this).text("登录中...").attr("disabled","disabled").addClass("layui-disabled");
	        setTimeout(function(){
	           $("#loginFrm").submit();
	        },1000);
	        return false;
	    })

	    //表单输入效果
	    $(".loginBody .input-item").click(function(e){
	        e.stopPropagation();
	        $(this).addClass("layui-input-focus").find(".layui-input").focus();
	    })
	    $(".loginBody .layui-form-item .layui-input").focus(function(){
	        $(this).parent().addClass("layui-input-focus");
	    })
	    $(".loginBody .layui-form-item .layui-input").blur(function(){
	        $(this).parent().removeClass("layui-input-focus");
	        if($(this).val() != ''){
	            $(this).parent().addClass("layui-input-active");
	        }else{
	            $(this).parent().removeClass("layui-input-active");
	        }
	    })
		
		
	    var mainIndex;
	    $("#signup").click(function(){
			//打开添加页面
				mainIndex=layer.open({
					
					  type: 2, 
					  title:'欢迎注册',
					  area:['450px','500px'],
					  content: '${ctx}/sys/toSignup.action' //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
				});
		    })

	})

	</script>
</body>
</html>