<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>个人资料</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="${ctx }/resources/layui/css/layui.css"
	media="all" />
	<link rel="stylesheet" href="${ctx }/resources/css/public.css"
	media="all" />
</head>
<body class="childrenBody">
<form class="layui-form layui-row" method="post" id="dataFrm" lay-filter="dataFrm">
	<div class="layui-col-md3 layui-col-xs12 user_right">
		<div class="layui-upload-list">
			<img class="layui-upload-img layui-circle userFaceBtn userAvatar" id="userFace">
			<!-- 保存当前显示图片的地址 -->
			<input type="hidden" name="headimg" id="headimg">
		</div>
		<button type="button" class="layui-btn layui-btn-primary userFaceBtn" id="uploadimg"><i class="layui-icon">&#xe67c;</i> 掐指一算，我要换一个头像了</button>
		<p>提示更改成功后,必须刷新页面才能看到左侧栏头像更新</p>
	</div>
	<div class="layui-col-md6 layui-col-xs12">
		<div class="layui-form-item">
			<div class="layui-input-block" >
				<input type="hidden" type="text" id="userid" name="userid" readonly="readonly" class="layui-input layui-disabled">
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">用户名</label>
			<div class="layui-input-block">
				<input type="text" id="loginname" name="loginname" disabled class="layui-input layui-disabled">
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">真实姓名</label>
			<div class="layui-input-block">
				<input type="text" id="realname" name="realname" placeholder="请输入真实姓名" lay-verify="required" class="layui-input realName">
			</div>
		</div>
		<div class="layui-form-item" pane="">
			<label class="layui-form-label">性别</label>
			<div class="layui-input-block userSex">
				<input type="radio" name="sex" value="1" title="男" checked="">
				<input type="radio" name="sex" value="0" title="女">
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">手机号码</label>
			<div class="layui-input-inline">
				<input type="tel" id="phone" name="phone" placeholder="请输入手机号码" lay-verify="phone" class="layui-input userPhone">
			</div>
			<label class="layui-form-label">职位</label>
			<div class="layui-input-inline">
				<input type="text"  name="position" placeholder="请输入职位" lay-verify="" class="layui-input">
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">邮箱</label>
			<div class="layui-input-block">
				<input type="text"   placeholder="请输入邮箱"  class="layui-input userEmail">
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">出生年月</label>
			<div class="layui-input-block">
				<input type="text" id="birthtime" name="birthtime" readonly placeholder="请输入出生年月" lay-verify="userBirthday"  class="layui-input userBirthday">
			</div>
		</div>
		
		<div class="layui-form-item">
			<label class="layui-form-label">爱好</label>
			<div class="layui-input-block userHobby">
				<input type="checkbox" name="like[吃饭]" title="吃饭">
				<input type="checkbox" name="like[睡觉]" title="睡觉">
				<input type="checkbox" name="like[玩游戏]" title="玩游戏">
				<input type="checkbox" name="like[看书]" title="看书">
				<input type="checkbox" name="like[健身]" title="健身">
				<input type="checkbox" name="like[钓鱼]" title="钓鱼">
				<input type="checkbox" name="like[打波]" title="打波">
				<input type="checkbox" name="like[听歌]" title="听歌">
				<input type="checkbox" name="like[旅行]" title="旅行">
				<input type="checkbox" name="like[看电影]" title="看电影">
			</div>
		</div>
		
		<div class="layui-form-item">
			<div class="layui-input-block">
				<button class="layui-btn" lay-submit="" lay-filter="changeUser">立即提交</button>
			</div>
		</div>
	</div>
</form>
<script type="text/javascript" src="${ctx }/resources/layui/layui.js"></script>
<script type="text/javascript">
layui.use([ 'jquery', 'layer', 'form', 'table','laydate','upload'], function() {
	var $ = layui.jquery;
	var layer = layui.layer;
	var form = layui.form;
	var table = layui.table;
	var laydate = layui.laydate;
	var upload = layui.upload;
	
	 //初始化图片
	   upload.render({
	        elem: '#uploadimg',
	        url: '${ctx}/file/uploadHeadPhoto.action',
	        method : "get",  //此处是为了演示之用，实际使用中请将此删除，默认用post方式提交
	        acceptMime: 'image/*',
	        field:'mf',
	        done: function(res, index, upload){
	            $('#userFace').attr('src',"${ctx}/file/downloadShowFile.action?path="+res.data.src);
	            $('#headimg').val(res.data.src);//显示图片地址
	        }
	    });

	
	//渲染时间
	laydate.render({
		elem:'#birthtime',
		type:'date'
	});

	//保存
	form.on("submit(changeUser)",function(obj){
		//序列化表单数据  序列化需要name属性来获取值
		var params=$("#dataFrm").serialize();
// 		alert(params);
		$.post("${ctx}/user/updateUserInfo.action",params,function(obj){
			alert(obj.msg);
		})
	});
	
	function queryUserInfo(){
		$.get("${ctx}/user/queryUserInfo.action",function(obj){
			//赋值
			form.val("dataFrm",obj);
			$('#userFace').attr('src',"${ctx}/file/downloadShowFile.action?path="+obj.headimg);
			})
		}
	queryUserInfo();

	
});
</script>

</body>
</html>