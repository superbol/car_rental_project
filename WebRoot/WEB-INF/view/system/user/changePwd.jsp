<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
<head>
	<meta charset="utf-8">
	<title>修改密码</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="${ctx }/resources/layui/css/layui.css"
	media="all" />
	<link rel="stylesheet" href="${ctx }/resources/css/public.css"
	media="all" />
</head>


<body class="childrenBody">
<form class="layui-form layui-row changePwd">
	<div class="layui-col-xs12 layui-col-sm6 layui-col-md6">
		<div class="layui-input-block layui-red pwdTips">请输入旧密码，新密码必须两次输入一致才能提交</div>
		<div class="layui-form-item">
			<label class="layui-form-label">用户名</label>
			<div class="layui-input-block">
				<input type="text" id="realname" disabled class="layui-input layui-disabled">
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">旧密码</label>
			<div class="layui-input-block">
				<input type="password" value="" id="oldpwd" name="oldpwd"  placeholder="请输入旧密码" lay-verify="required|oldPwd" class="layui-input pwd">
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">新密码</label>
			<div class="layui-input-block">
				<input type="password" value="" id="pwd" name="pwd" placeholder="请输入新密码" lay-verify="required|newPwd" id="oldPwd" class="layui-input pwd">
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">确认密码</label>
			<div class="layui-input-block">
				<input type="password" value="" placeholder="请确认密码" lay-verify="required|confirmPwd" class="layui-input pwd">
			</div>
		</div>
		<div class="layui-form-item">
			<div class="layui-input-block">
				<button class="layui-btn layui-btn-disabled" id="changePwd"  lay-submit="" lay-filter="changePwd">立即修改</button>
				<button type="reset" class="layui-btn layui-btn-primary">重置</button>
			</div>
		</div>
	</div>
</form>
<script type="text/javascript" src="${ctx }/resources/layui/layui.js"></script>
<script type="text/javascript" >
layui.use(['form','layer','laydate','table','laytpl'],function(){
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        laydate = layui.laydate,
        laytpl = layui.laytpl,
        table = layui.table;
	
    //添加验证规则
    form.verify({

        newPwd : function(value, item){
            if(value.length < 6){
                return "密码长度不能小于6位";
            }
        },
        confirmPwd : function(value, item){
            if(!new RegExp($("#pwd").val()).test(value)){
                return "两次输入密码不一致，请重新输入！";
            }
        }
    })

    //保存
	form.on("submit(changePwd)",function(obj){
		//序列化表单数据  序列化需要name属性来获取值
		var pwd = $('#pwd').val();//获取文本框的值
		$.post("${ctx}/user/updatePwd.action",{pwd:pwd},function(obj){
			layer.msg(obj.msg, {icon: 1});
		})
	});

	$(function(){
		//鼠标移开事件
		$('#oldpwd').blur(function(){
			var oldpwd = $('#oldpwd').val();//获取文本框的值
			$.post("${ctx}/user/queryPwd.action",{pwd:oldpwd},function(obj){
				if(obj.msg=="密码正确"){
			 	    layer.msg(obj.msg, {icon: 1});
					$("#changePwd").attr("class","layui-btn");
					}else{
			 	    layer.msg(obj.msg, {icon: 5});
					$("#changePwd").attr("class","layui-btn layui-btn-disabled");
						}
				})
			})
		})
	//查询用户名
	function queryUserInfo(){
	$.get("${ctx}/user/queryUserInfo.action",function(obj){
		//赋值
		$("#realname").attr("value",obj.realname);//填充内容 
		})
		}
	queryUserInfo();
})
</script>
</body>
</html>